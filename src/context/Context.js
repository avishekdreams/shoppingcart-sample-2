import { createContext, useContext, useReducer } from "react";
import { cartReducer, productReducer } from "./Reducers";

const Cart = createContext();

const Context = ({ children }) => {
    const [state, dispatch] = useReducer(cartReducer, {
        cart: [],
    });

    const [prodState, prodDispatch] = useReducer(productReducer, {
        byRating: 0,
        searchQuery: ""
    });

    return (
        <Cart.Provider value={{ state, dispatch, prodState, prodDispatch }}>
            {children}
        </Cart.Provider>
    );
}

export const CartState = () => {
    return useContext(Cart);
};

export default Context;