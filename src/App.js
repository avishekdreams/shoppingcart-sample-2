import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import Header from './components/Header';
import Home from './components/Home';
import Cart from './components/Cart';
import './App.css';

const queryClient = new QueryClient();

function App() {
	return (
		<div className="App">
			<BrowserRouter>
				<Header />
				<div>
					<QueryClientProvider client={queryClient}>
						<Routes>
							<Route path="/" element={<Home />} />
							<Route path="/cart" element={<Cart />} />
						</Routes>
						<ReactQueryDevtools />
					</QueryClientProvider>
				</div>
			</BrowserRouter>
		</div>
	);
}

export default App;