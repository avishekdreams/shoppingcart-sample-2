import axios from 'axios';

export function getAllProducts(page) {
    console.log(page);
    return axios.get(`https://dummyjson.com/products?limit=9&skip=${page}`, { params: { _sort: "id" } }).then(result => result.data);
}