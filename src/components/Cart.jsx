import React, { useEffect, useState } from 'react';
import { Button, Col, FormControl, Image, ListGroup, ListGroupItem, Row } from 'react-bootstrap';
import { AiFillDelete } from 'react-icons/ai';
import { CartState } from '../context/Context';
import Rating from './Rating';

export default function Cart() {
    const [total, setTotal] = useState(0);
    const { state: { cart }, dispatch } = CartState();

    useEffect(() => {
        setTotal(cart.reduce((total, current) => total + Number(current.price) * current.qty, 0));
    }, [cart]);

    return (
        <div className="home">
            <div className="productContainer">
                <ListGroup>
                    {cart.map((prod) => (
                        <ListGroupItem key={prod.id}>
                            <Row>
                                <Col md={2}>
                                    <Image src={prod.thumbnail} alt={prod.title} fluid rounded />
                                </Col>
                                <Col md={2}><span>{prod.title}</span></Col>
                                <Col md={2}><span>{prod.price}</span></Col>
                                <Col md={2}><Rating rating={prod.rating} /></Col>
                                <Col md={2}>
                                    <FormControl 
                                        as="select" 
                                        value={prod.stock}
                                        onChange={(e) => dispatch({
                                            type: "CHANGE_CART_QTY",
                                            payload: {
                                                id: prod.id,
                                                qty: e.target.value
                                            }
                                        })}
                                    >
                                        {[...Array(5).keys()].map((x) => (
                                            <option key={x + 1}>{x + 1}</option>
                                        ))}
                                    </FormControl>
                                </Col>
                                <Col md={2}>
                                    <Button
                                        type="button"
                                        variant="light"
                                        onClick={() => dispatch({ type: "REMOVE_FROM_CART", payload: prod })}
                                    >
                                        <AiFillDelete fontSize="20px" />
                                    </Button>
                                </Col>
                            </Row>
                        </ListGroupItem>
                    ))}
                </ListGroup>
            </div>
            <div className="filters summary">
                <span className="title"> Subtotal ({cart.length}) items </span>
                <span style={{ fontWeight: 700, fontSize: 20 }}>Total: Rs. {total}</span>
                <Button type="button" disabled={cart.length === 0}>Proceed to Checkout</Button>
            </div>
        </div>
    )
}