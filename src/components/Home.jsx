import React, { useState } from 'react';
import SingleProduct from './SingleProduct';
import './styles.css';
import Filters from './Filters';
import { CartState } from "../context/Context";
import { useQuery } from '@tanstack/react-query';
import { getAllProducts } from './../apis/products';
import CustomPagination from './CustomPagination';

export default function Home() {
    const { state: { products }, prodState: { sort, byRating, searchQuery }, dispatch } = CartState();
    const [page, setPage] = useState(1);

    const addToCartHandler = (e) => {
        dispatch({ type: "ADD_TO_CART", payload: e });
    }

    const removeFromCartHandler = (e) => {
        dispatch({ type: "REMOVE_FROM_CART", payload: e });
    }

    const { error, isLoading, data } = useQuery({
        queryKey: ['products', page],
        queryFn: () => getAllProducts(`${page * 10 - 10}`),
        keepPreviousData: true
    });

    const onPageChanged = (currPage) => {
        if (currPage >= 1 && currPage <= data?.total && currPage !== page) {
            setPage(currPage);
        }
    };

    const onImgLoadComplete = (index) => {
        if (index === data.products.length - 1) {
            console.log("loaded");
        }
    };

    const transformProds = () => {
        let sortedProds = data?.products;

        if (sort) {
            sortedProds = sortedProds.sort((a, b) => sort === 'lowToHigh' ? a.price - b.price : b.price - a.price);
        }

        if (byRating) {
            sortedProds = sortedProds.filter((prod) => Math.floor(prod.rating) >= byRating);
        }

        if (searchQuery) {
            sortedProds = sortedProds.filter((prod) => prod.title.toLowerCase().includes(searchQuery));
        }
        return sortedProds;
    }

    if (isLoading) {
        return <h2>Loading...</h2>
    }

    return (
        <div className='homeContainer'>
            <div className='home'>
                <Filters />
                <div className='productContainer'>
                    {transformProds().map((item, index) => (
                        <SingleProduct prod={item} key={item.id} addToCartHandler={(e) => addToCartHandler(e)} removeFromCartHandler={(e) => removeFromCartHandler(e)} index={index} onImgLoadComplete={(e) => onImgLoadComplete(e)}/>
                    ))}
                </div>
            </div>
            {data?.total && (
                <CustomPagination
                    total={data?.total}
                    pageSize={10}
                    onPageChanged={(e) => onPageChanged(e)}
                    current={page}
                />
            )}
        </div>
    )
}