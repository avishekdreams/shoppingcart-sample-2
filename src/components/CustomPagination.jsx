import { Pagination } from "antd";

export default function CustomPagination(props) {
    return (
        <div className="pagination">
            <Pagination
                total={props.total}
                onChange={(value) => props.onPageChanged(value)}
                current={props.current}
                showSizeChanger={false}
            />
        </div>
    );

}