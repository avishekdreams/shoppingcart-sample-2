import React from 'react';
import { Container, Navbar, Form, Nav, Dropdown, Badge, Button, FormControl } from 'react-bootstrap';
import { AiFillDelete } from 'react-icons/ai';
import { FaShoppingCart } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import { CartState } from '../context/Context';

export default function Header() {

    const { state: { cart }, dispatch, prodDispatch } = CartState();

    return (
        <Navbar bg="dark" variant="dark">
            <Container>
                <Navbar.Brand style={{ height: 80 }}>
                    <Link to="/">Shopping Cart</Link>
                </Navbar.Brand>
                <Form className="d-flex">
                    <FormControl
                        style={{ width: 500 }}
                        placeholder="Search a product"
                        className="m-auto"
                        onChange={(e) => {
                            prodDispatch({
                                type: 'FILTER_BY_SEARCH',
                                payload: e.target.value
                            })
                        }}
                    />
                </Form>
                <Nav>
                    <Dropdown>
                        <Dropdown.Toggle variant="success">
                            <FaShoppingCart color="white" fontSize="25px" />
                            <Badge bg="success">{cart.length}</Badge>
                        </Dropdown.Toggle>
                        <Dropdown.Menu style={{ minWidth: 370 }}>
                            {cart.length > 0 ? (
                                <>
                                    {cart.map((prod) => (
                                        <span className="cartItem" key={prod.id}>
                                            <img className='cartItemImg' src={prod.thumbnail} alt={prod.title} />
                                            <div className="cartItemDetail">
                                                <span>{prod.title}</span>
                                                <span>Rs. {prod.price}</span>
                                            </div>
                                            <AiFillDelete
                                                fontSize="20px"
                                                style={{ cursor: "pointer" }}
                                                onClick={() => dispatch({
                                                    type: "REMOVE_FROM_CART",
                                                    payload: prod
                                                })}
                                            />
                                        </span>
                                    ))}
                                    <Link to="/cart">
                                        <Button style={{ width: "95%", margin: "0 10px" }}>Got to Cart</Button>
                                    </Link>
                                </>
                            ) : (
                                <span style={{ padding: 10 }}>Cart is empty!</span>
                            )}

                        </Dropdown.Menu>
                    </Dropdown>
                </Nav>

            </Container>
        </Navbar >
    )
}