import { Button, Card } from "react-bootstrap";
import { CartState } from '../context/Context';
import Rating from "./Rating";

export default function SingleProduct({ prod, addToCartHandler, removeFromCartHandler, onImgLoadComplete, index }) { 

    const { state: { cart } } = CartState();

    return (
        <div className="products">
            <Card>
                <Card.Img variant="top" src={prod.thumbnail} alt={prod.title} style={{ width: '100%', height: '10vw', objectFit: 'cover' }} onLoad={() => onImgLoadComplete(index)}/>
                <Card.Body>
                    <Card.Title>{prod.title}</Card.Title>
                    <Card.Subtitle style={{ paddingBottom: 10 }}>
                        <span>Rs. {prod.price}</span><br />
                        <Rating rating={Math.floor(prod.rating)} />
                    </Card.Subtitle>
                    {
                        cart.some((p) => p.id === prod.id) ? (
                            <Button onClick={() => removeFromCartHandler(prod)} variant="danger">Remove From Cart</Button>
                        ) : (
                            <Button onClick={() => addToCartHandler(prod)}>Add To Cart</Button>
                        )
                    }
                </Card.Body>
            </Card>
        </div>
    )
}